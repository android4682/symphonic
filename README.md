# Symphonic

Symphonic *(relating to or having the form or character of a symphony)*.

A webserver with autowiring (dependency injection) and decorator routing.

## Table of contents

- [Symphonic](#symphonic)
  - [Table of contents](#table-of-contents)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Post installation](#post-installation)
  - [Example usage (from this repo)](#example-usage-from-this-repo)
  - [API](#api)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [Authors](#authors)
  - [License](#license)
  - [Special thanks](#special-thanks)

## Prerequisites

This project requires NodeJS (version 18.13.0 or later) and NPM (or Yarn).
[Node](http://nodejs.org/) and [NPM](https://npmjs.org/) (or [Yarn](https://yarnpkg.com/)) are really easy to install.
To make sure you have them available on your machine, try running the following command.

For `node`:
```sh
$ node -v
v18.13.0
```

For `npm`:
```sh
$ npm -v
8.19.3
```

For `yarn`:
```sh
$ yarn -v
1.22.19
```

## Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)

Before you can install this package, make sure you added my repository to your `.npmrc` or `.yarnrc`:

`.npmrc`
```
@android4682:registry=https://gitlab.com/api/v4/packages/npm/
```

`.yarnrc`
```
"@android4682:registry" "https://gitlab.com/api/v4/packages/npm/"
```

To install and set up the library, run:

```
$ npm install -S @android4682/symphonic
```

Or if you prefer using `yarn`:

```
$ yarn add @android4682/symphonic
```

## Post installation

To get this setup for your custom project, create a file in your project with the following content:
```ts
// index.ts
import { Kernel } from "@android4682/symphonic"

new Kernel().init()
```

**(Optional)** If you want you can make a `autowire_config.json` file:
```json
{
  "services": [],
  "controllers": []
}
```

You probably want a `.env` file:
```
loglevel="5"
autowire_config_filepath="{{CWD}}/autowire_config.json"
port="8888"
hostname="127.0.0.1"
```

If you created a `autowire_config.json` make sure the `autowire_config_filepath` in `.env` points towards your created file.

You can use `{{CWD}}` to insert the current working directory.

Now you're ready to boot it up! Run `ts-node index.ts` (or set this in your `package.json` under scripts and run that) and see the webserver start!

If you need a live example see the next chapter.

## Example usage (from this repo)

If you clone this repository, you can run the following commands to run the an example.

1. Rename `.env.example` to `.env`.
2. Run the following commands:

Via `npm`:
```sh
npm install --include=dev
npm run dev
```

Or for `yarn`:
```sh
yarn install
yarn dev
```

3. Go to [http://127.0.0.1:8888/get](http://127.0.0.1:8888/get) (default) to see the auto routing.

This controller is located at `specs/Helper/Controller/TestController.ts`.

## API

*Coming soon!*

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

But to summarize it:

1.  Fork it!
2.  Create your feature branch: `git checkout -b my-new-feature`
3.  Add your changes: `git add .`
4.  Commit your changes: `git commit -am 'Add some feature'`
    -  *Your commit message must follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) rules.*
5.  Push to the branch: `git push origin my-new-feature`
6.  Submit a pull request :sunglasses:

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **Andrew de Jong** - *Lead Developer* - [android4682](https://gitlab.com/android4682)

## License

[Creative Commons Attribution 4.0 International](https://gitlab.com/android4682/symphonic/-/blob/master/LICENSE) © Andrew de Jong

## Special thanks

* **Andrea Sonny** - For a [`README.md` template](https://gist.github.com/andreasonny83/7670f4b39fe237d52636df3dec49cf3a) - [andreasonny83](https://gist.github.com/andreasonny83)
* **Billie Thompson** - For a [`CONTRIBUTING.md` template](https://gist.github.com/PurpleBooth/b24679402957c63ec426) - [PurpleBooth](https://gist.github.com/PurpleBooth)
