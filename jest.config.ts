import type { Config } from "@jest/types"

const config: Config.InitialOptions = {
  preset: "ts-jest",
  testEnvironment: "node",
  automock: false,
  testRegex: ".\/specs\/.*\.(?:spec|test)\.(?:t|j)s$",
  coverageReporters: [
    'html',
    'text',
    'text-summary',
    'cobertura'
  ],
  coverageThreshold: {
    "global": {
      "branches": 60,
      "functions": 80,
      "lines": 70,
      "statements": 70
    }
  },
  collectCoverageFrom: [
    "./src/**/*.(t|j)s",
    "!./src/3. Infrastructure/index.ts",
    "!./src/Test.ts",
    "!./src/Domain/**"
  ],
  moduleDirectories: [
    'node_modules',
    'src'
  ],
  moduleFileExtensions: [
    "ts",
    "js",
    "json"
  ],
  transform: {
    "^.+\\.(t|j)s$": [
      'ts-jest',
      {
        tsconfig: './specs/tsconfig.json'
      }
    ]
  },
  coverageDirectory: "./coverage",
  coverageProvider: "babel"
}
export default config
