import { shouldRunSuite } from "../../../../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../../../../Helper/PublicKernel";
import { Container } from "../../../../../../src/2. Application/DependencyInjection/Container";
import { DefaultSessionManagerHelper } from "../../../../../Helper/Mocks/SessionManager";
import { SessionMock } from "../../../../../Helper/Mocks/Session";
import { isDeepInstanceOf, randomStringGenerator } from "@android4682/typescript-toolkit";
import { DefaultSession } from "../../../../../../src/2. Application/Abstract/Session/DefaultSession";
import { BetterConsole } from "@android4682/better-console";
import { Session } from "../../../../../../src/1. Domain/Interfaces/Session";

if (shouldRunSuite('UNIT')) {
  let tinyKernel = new KernelMock()
  let container = new Container(new BetterConsole())
  beforeAll(() => new Promise<void>((resolve, reject) => KernelFactory.generateTinyKernel(true, true, true).then((kernel) => {
    tinyKernel = kernel
    container = kernel.getContainer()
    container.registerService(SessionMock, 'session', true, true)

    resolve()
  }).catch((e) => reject(e))))

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('DefaultSessionMananger', () => {
    it('should be initializeble through a helper', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)

      expect(sessionManager).not.toBe(undefined)
    })

    it('should be have a session class name', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)

      expect(typeof sessionManager.sessionClassName).toBe('string')
    })

    it('should be able to create a session', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)
      let session = sessionManager.createSession()

      expect(isDeepInstanceOf(session, DefaultSession)).toBe(true)
    })

    it('should be able to update a created session', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)
      let session = sessionManager.createSession({foo: 'bar'})

      expect(session.session.foo).toBe('bar')
      sessionManager.updateSession(session.uid, {foo: 'tymon'})
      expect(session.session.foo).toBe('tymon')
    })

    it('should be able to create a session through update when requested', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)
      let uid = randomStringGenerator(64)

      let session = sessionManager.updateSession(uid, {foo: 'tymon'}, true)
      expect(session).not.toBe(false)
      session = <Session> session
      expect(session.session.foo).toBe('tymon')
    })

    it('should not create a session through update when not requested', () => {
      let sessionManager = new DefaultSessionManagerHelper(container)
      let uid = randomStringGenerator(64)

      let session = sessionManager.updateSession(uid, {foo: 'tymon'})
      expect(session).toBe(false)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('DefaultSessionMananger', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}