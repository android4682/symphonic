import { shouldRunSuite } from "../../../SuiteRunChecker";
import { CacheMock } from "../../../Helper/Mocks/CacheMock";

if (shouldRunSuite('UNIT')) {
  describe('Cache', () => {
    it('should be initializeble through a helper', () => {
      let cache = new CacheMock()

      expect(cache).not.toBe(undefined)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Cache', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
