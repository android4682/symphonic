import { shouldRunSuite } from "../../../SuiteRunChecker";
import { SymphonicMemoryCaching } from "../../../../src/2. Application/Cache/MemoryCaching";
import { randomStringGenerator } from "@android4682/typescript-toolkit";

if (shouldRunSuite('UNIT')) {
  describe('SymphonicMemoryCaching', () => {
    it('should be initializeble', () => {
      let cache = new SymphonicMemoryCaching()

      expect(cache).toBeInstanceOf(SymphonicMemoryCaching)
    })

    it('should be able to get and set a variable', () => {
      let cache = new SymphonicMemoryCaching()
      let key = randomStringGenerator(8)
      let nValue = randomStringGenerator(8)

      expect(cache.get(key)).toBe(undefined)
      cache.set(key, nValue)
      expect(cache.get(key)).toBe(nValue)
    })

    it('should be able to get full original cache', () => {
      let cache = new SymphonicMemoryCaching()

      let key = randomStringGenerator(8)
      let nValue = randomStringGenerator(8)

      cache.set(key, nValue)
      let cacheData = cache.getOriginalCachingSystem()
      expect(cacheData[key]).toBe(nValue)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('SymphonicMemoryCaching', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
