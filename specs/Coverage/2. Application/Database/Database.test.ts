import { shouldRunSuite } from "../../../SuiteRunChecker";
import { DatabaseMock } from "../../../Helper/Mocks/DatabaseMock";
import { Request } from "express";

if (shouldRunSuite('UNIT')) {
  describe('Database', () => {
    it('should be initializeble through helper', () => {
      let database = new DatabaseMock()

      expect(database).toBeInstanceOf(DatabaseMock)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Database', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
