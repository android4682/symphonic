import { shouldRunSuite } from "../../../SuiteRunChecker";
import { SymphonicMemoryDatabase } from "../../../../src/2. Application/Database/MemoryDatabase";

if (shouldRunSuite('UNIT')) {
  describe('SymphonicMemoryDatabase', () => {
    it('should be initializeble', () => {
      let database = new SymphonicMemoryDatabase()

      expect(database).toBeInstanceOf(SymphonicMemoryDatabase)
    })

    it('should be able to get original database handler', () => {
      let database = new SymphonicMemoryDatabase()

      expect(typeof database.getOriginalDatabaseHandler()).toBe('object')
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('SymphonicMemoryDatabase', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
