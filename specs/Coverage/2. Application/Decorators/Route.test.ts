import "reflect-metadata"
import { shouldRunSuite } from "../../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../../Helper/PublicKernel";
import { METADATA_METHODS, METADATA_ROUTE } from "../../../../src/2. Application/Constants";
import { RouteDummy } from "../../../Helper/RouteDummy";

if (shouldRunSuite('UNIT')) {
  let tinyKernel = new KernelMock()
  beforeAll(() => new Promise<void>((resolve, reject) => KernelFactory.generateTinyKernel(true, true, true).then((kernel) => {
    tinyKernel = kernel
    
    resolve()
  }).catch((e) => reject(e))))

  describe('Route', () => {
    it('should be able to add default method route metadata to controller methods when not specified', () => {
      expect(Reflect.getMetadata(METADATA_ROUTE, RouteDummy.prototype, 'catchAllRoute')).toBe('*')
      expect(Reflect.getMetadata(METADATA_METHODS, RouteDummy.prototype, 'catchAllRoute')).toMatchObject(['all'])
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Route', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}