import { shouldRunSuite } from "../../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../../Helper/PublicKernel";
import { Container } from "../../../../src/2. Application/DependencyInjection/Container";
import { SpecialMap, randomStringGenerator } from "@android4682/typescript-toolkit";
import { MissingService } from "../../../../src/2. Application/Errors/MissingService";
import { DummyService } from "../../../Helper/DummyService";
import { StevenHe } from "../../../Helper/Failure";
import { FailedToAutowire } from "../../../../src/2. Application/Errors/FailedToAutowire";
import { KernelManager } from "../../../../src/2. Application/Manager/KernelManager";
import { ClassObject } from "../../../../src/1. Domain/Interfaces/ClassObject";
import { EnvManager } from "../../../../src/2. Application/Manager/EnvManager";

if (shouldRunSuite('UNIT')) {
  let tinyKernel = new KernelMock()
  beforeAll(() => new Promise<void>((resolve, reject) => KernelFactory.generateTinyKernel(true, true, true).then((kernel) => {
    tinyKernel = kernel
    
    resolve()
  }).catch((e) => reject(e))))

  describe('Container', () => {
    it('should, when a missing service is requested, throw a tantrum by default', () => {
      let container: Container = new Container(tinyKernel.getLogger())

      expect(container.get.bind(container, randomStringGenerator(12))).toThrowError(MissingService)
    })

    it('should, when a missing service is requested, NOT throw a tantrum when asked', () => {
      let container: Container = new Container(tinyKernel.getLogger())

      expect(container.get(randomStringGenerator(12), false)).toBe(false)
    })

    it('isServiceStatic method should return true when given a static class', () => {
      let container: Container = new Container(tinyKernel.getLogger())

      expect(container.isServiceStatic(DummyService)).toBe(true)
    })

    it('isServiceStatic method should return false when given a constructed class', () => {
      let container: Container = new Container(tinyKernel.getLogger())
      let constructed = new DummyService()

      expect(container.isServiceStatic(constructed)).toBe(false)
    })

    it('should not be able to register a static service under an already registered service name', () => {
      let container: Container = new Container(tinyKernel.getLogger())
      let serviceName: string = randomStringGenerator(12)

      expect(container.registerService(DummyService, serviceName)).toBe(true)
      expect(container.registerService(DummyService, serviceName)).toBe(false)
    })

    it('should not be able to register a constructed service under an already registered service name', () => {
      let container: Container = new Container(tinyKernel.getLogger())
      let serviceName: string = randomStringGenerator(12)
      let constructedService = new DummyService()

      expect(container.registerService(constructedService, serviceName)).toBe(true)
      expect(container.registerService(constructedService, serviceName)).toBe(false)
    })

    it('should fail to auto wire a service which requires another services that hasn\'t been registered yet', () => {
      let container: Container = new Container(tinyKernel.getLogger())
      let serviceName: string = randomStringGenerator(12)

      expect(container.isServiceStatic(StevenHe)).toBe(true)
      expect(container.registerService(StevenHe, serviceName)).toBe(false)
    })

    it('should fail getParameterTypes from a non-autowired service', () => {
      let container: Container = new Container(tinyKernel.getLogger())

      expect(container.getParameterTypes.bind(container, StevenHe, 'calculator')).toThrowError(FailedToAutowire)
    })

    it('should be able to mass construct services', () => {
      let container: Container = tinyKernel.getContainer()
      let services = new SpecialMap<string, ClassObject<any>>([
        ['KernelManager', KernelManager],
        ['Dummy', DummyService]
      ])

      let result_services = container.constructServices(services)
      expect(result_services.get('KernelManager')).toBeInstanceOf(KernelManager)
      expect(result_services.get('Dummy')).toBeInstanceOf(DummyService)
    })

    it('should be able to return a list of services that are an instance of a given class', () => {
      let container: Container = tinyKernel.getContainer()
      let services = container.getServicesThatAreAnInstanceOf(EnvManager)

      expect(services.size).toBeGreaterThan(0)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Container', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}