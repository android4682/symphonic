import { shouldRunSuite } from "../../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../../Helper/PublicKernel";
import { SpecialMap } from "@android4682/typescript-toolkit";
import { RouteContainer } from "../../../../src/2. Application/DependencyInjection/RouteContainer";
import { ClassObject } from "../../../../src/1. Domain/Interfaces/ClassObject";
import { TestController } from "../../../Helper/Controller/TestController";
import { MissingRoute } from "../../../../src/2. Application/Errors/MissingRoute";

if (shouldRunSuite('UNIT')) {
  let tinyKernel = new KernelMock()
  beforeAll(() => new Promise<void>((resolve, reject) => KernelFactory.generateTinyKernel(true, true, true).then((kernel) => {
    tinyKernel = kernel
    
    resolve()
  }).catch((e) => reject(e))))

  describe('RouteContainer', () => {
    it('should be able to find a valid route', () => {
      let controllers = new SpecialMap<string, ClassObject<any>>([
        ['Test', TestController],
        ['SomethingElse', TestController]
      ])

      let routeContainer = new RouteContainer(tinyKernel.getLogger(), tinyKernel.getContainer(), controllers)

      expect(routeContainer.findRouteHandler.bind(routeContainer, '/get', 'get')).not.toThrowError(MissingRoute)
    })

    it('should be throw a MissingRoute error if the route doesn\'t exist', () => {
      let controllers = new SpecialMap<string, ClassObject<any>>([
        ['Test', TestController],
        ['SomethingElse', TestController]
      ])

      let routeContainer = new RouteContainer(tinyKernel.getLogger(), tinyKernel.getContainer(), controllers)

      expect(routeContainer.findRouteHandler.bind(routeContainer, '/failure', 'get')).toThrowError(MissingRoute)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('RouteContainer', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}