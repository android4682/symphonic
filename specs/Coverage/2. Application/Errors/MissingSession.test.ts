import { randomStringGenerator } from "@android4682/typescript-toolkit";
import { MissingSession } from "../../../../src/2. Application/Errors/MissingSession";
import { shouldRunSuite } from "../../../SuiteRunChecker";

if (shouldRunSuite('UNIT')) {
  describe('MissingSession', () => {
    it('should be able to be made', () => {
      let uid = randomStringGenerator(32)
      let error = new MissingSession(uid)
      expect(error).toBeInstanceOf(MissingSession)
      expect(error.errorName).toBe(MissingSession.name)
      expect(error.message).toBe(`Couldn't find session by uid '${uid}'.`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('MissingSession', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
