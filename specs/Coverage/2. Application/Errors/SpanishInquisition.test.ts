import { SpanishInquisition } from "../../../../src/2. Application/Errors/SpanishInquisition";
import { shouldRunSuite } from "../../../SuiteRunChecker";

if (shouldRunSuite('UNIT')) {
  describe('SpanishInquisition', () => {
    it('should be able to be made', () => {
      let error = new SpanishInquisition()
      expect(error).toBeInstanceOf(SpanishInquisition)
      expect(error.errorName).toBe(SpanishInquisition.name)
      expect(error.message).toBe('NOBODY EXPECTS THE SPANISH INQUISITION!')
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('SpanishInquisition', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}