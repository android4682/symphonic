import { shouldRunSuite } from "../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../Helper/PublicKernel";
import { NotInitiated } from "../../../src/2. Application/Errors/NotInitiated";
import fs from "node:fs";
import path from "node:path";
import { Webserver } from "../../../src/3. Infrastructure/Webserver/Webserver";
import { BetterConsole } from "@android4682/better-console";
import { EnvManager } from "../../../src/2. Application/Manager/EnvManager";
import { SymphonicMemoryDatabase } from "../../../src/2. Application/Database/MemoryDatabase";
import { EnvManagerMock } from "../../Helper/Mocks/EnvManager";
import { FixTableIntegertyLevel } from "../../../src/1. Domain/Interfaces/BasicDatabaseHandler";

if (shouldRunSuite('UNIT')) {
  afterEach(() => {
    jest.clearAllMocks()
  })
  
  describe('Kernel', () => {
    it('should throw an NotInitiated when getting Container before initializing', () => {
      let kernel: KernelMock = new KernelMock()

      expect(kernel.getContainer.bind(kernel)).toThrowError(NotInitiated)
    })

    it('should throw an NotInitiated when getting KernelManager before initializing', () => {
      let kernel: KernelMock = new KernelMock()

      expect(kernel.getKernelManager.bind(kernel)).toThrowError(NotInitiated)
    })

    it('should throw an NotInitiated when getting Logger before initializing', () => {
      let kernel: KernelMock = new KernelMock()

      expect(kernel.getLogger.bind(kernel)).toThrowError(NotInitiated)
    })

    it('should have a logger with a default log level of 3 when initiated without envoirment variables', () => {
      let kernel: KernelMock = new KernelMock()
      let logger = kernel.initiatedBetterConsole()
      
      expect(logger).toBeInstanceOf(BetterConsole)
      expect(logger.logLevel).toBe(3)
    })

    it('should be able to load .env file', () => {
      return new Promise<void>((resolve, reject) => {
        KernelFactory.generateKernel().then((kernel) => {
          let tmpEnvFileName = `.env.${Date.now()}.bak`;
          let tmpEnvFilePath = path.join(process.cwd(), tmpEnvFileName)
          let envFilePath = path.join(process.cwd(), '.env')
          let needBackup = fs.existsSync(envFilePath)
          if (needBackup) fs.copyFileSync(envFilePath, tmpEnvFilePath)
          fs.writeFileSync(envFilePath, `loglevel="5"
autowire_config_filepath="{{CWD}}/autowire_config.json"
port="8888"
hostname="127.0.0.1"`)
          let envManager = kernel.original_initiateEnvManager()
          if (needBackup) fs.copyFileSync(tmpEnvFilePath, envFilePath)
          fs.rmSync((needBackup) ? tmpEnvFilePath : envFilePath)
          expect(envManager.loglevel).not.toBe(undefined)
          expect(envManager.autowire_config_filepath).not.toBe(undefined)
          expect(envManager.port).not.toBe(undefined)
          expect(envManager.hostname).not.toBe(undefined)

          resolve()
        }).catch((e) => reject(e))
      })
    })

    it('should be able to initialize the webserver', () => {
      return new Promise<void>((resolve, reject) => {
        KernelFactory.generateKernel().then(async (kernel) => {
          kernel.initiatedEnvManager()
          kernel.initiatedBetterConsole()
          kernel.initiatedKernelManager()
          await kernel.loadAutoWireConfig()
          await kernel.loadAutoWireableServices()
          kernel.original_createWebServer()

          let webserverInitiateMock = jest.spyOn(Webserver.prototype, 'initiate')
          let webserver = await kernel.original_initiateWebServer()
          
          expect(webserver).toBeInstanceOf(Webserver)
          expect(webserverInitiateMock).toHaveBeenCalled()

          await webserver.stopServer()
          resolve()
        }).catch((e) => reject(e))
      })
    })

    it('should be able to get original env', () => {
      return new Promise<void>((resolve, reject) => {
        KernelFactory.generateKernel().then((kernel) => {
          expect(kernel.original_getEnv()).toBeInstanceOf(EnvManager)

          resolve()
        }).catch((e) => reject(e))
      })
    })

    it('should not be able to get original env if not initiated', () => {
      return new Promise<void>((resolve, reject) => {
        let kernel = new KernelMock()
        expect(kernel.original_getEnv.bind(kernel)).toThrowError(NotInitiated)

        resolve()
      })
    })

    it('should fail to initiate database when fail', () => {
      return new Promise<void>((resolve, reject) => {
        let spy = jest.spyOn(SymphonicMemoryDatabase.prototype, 'checkTableIntegerty')
        spy.mockImplementation((fixLevel) => {
          return false
        })

        KernelFactory.generateTinyKernel().then(async (kernel) => {
          kernel.initiatedKernelManager()
          await kernel.loadAutoWireConfig()
          await kernel.loadAutoWireableServices()
          await kernel.initiateCaching()

          await expect(kernel.initiateDatabase.bind(kernel)).rejects.toThrowError('Database table intergerty is incorrect, if you want to auto fix this add \'--fix-db\' to the start command. This will try to auto fix the database integerty issues in a safe manner (no data loss). See the above error for more information.')

          let spyGetEnv = jest.spyOn(kernel, 'getEnv')
          spyGetEnv.mockImplementationOnce(() => {
            let envManager = new EnvManagerMock()
            let spyEnvManager = jest.spyOn(envManager, 'database_fix_table_integerty', 'get')

            spyEnvManager.mockImplementation(() => FixTableIntegertyLevel.safe)

            return envManager
          })

          await expect(kernel.initiateDatabase.bind(kernel)).rejects.toThrowError('Database table intergerty is incorrect, this might be fixable by replace \'--fix-db\' with \'--force-fix-db\' but, WARNING, this might result in a loss of data. See the above error for more information.')

          spyGetEnv = jest.spyOn(kernel, 'getEnv')
          spyGetEnv.mockImplementationOnce(() => {
            let envManager = new EnvManagerMock()
            let spyEnvManager = jest.spyOn(envManager, 'database_fix_table_integerty', 'get')

            spyEnvManager.mockImplementation(() => FixTableIntegertyLevel.force)

            return envManager
          })

          await expect(kernel.initiateDatabase.bind(kernel)).rejects.toThrowError('Database table intergerty is incorrect and could not be fixed. See the above error for more information.')

          resolve()
        }).catch((e) => reject(e))
      })
    })

    it('should be able to validate the compiled code', () => {
      let kernel: KernelMock = new KernelMock()

      expect(kernel.initTest.bind(kernel, false)).not.toThrowError()
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Kernel', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}