import { shouldRunSuite } from "../../../SuiteRunChecker";
import { KernelManager } from "../../../../src/2. Application/Manager/KernelManager";
import { NotInitiated } from "../../../../src/2. Application/Errors/NotInitiated";
import { BetterConsole } from "@android4682/better-console";
import path, { resolve } from "path";
import { rejects } from "assert";
import { InifinityObject, SpecialMap } from "@android4682/typescript-toolkit";
import { DummyService } from "../../../Helper/DummyService";
import { StevenHe } from "../../../Helper/Failure";

if (shouldRunSuite('UNIT')) {
  describe('KernelManager', () => {
    const silentLogger = new BetterConsole(0)

    afterEach(() => {
      jest.clearAllMocks()
    })

    it('should throw an error when auto wire config is requested but not initiated', () => {
      let kernelManager = new KernelManager(silentLogger)

      expect(kernelManager.getAutoWireConfig.bind(this)).toThrowError(NotInitiated)
    })

    it('should return default live auto wire config if no auto wire config file path is set in env', () => {
      return new Promise<void>((resolve, rejects) => {
        let kernelManager = new KernelManager(silentLogger)
        kernelManager.getLiveAutoWireConfig().then((autoWireConfig) => {
          expect(autoWireConfig).toMatchObject({
            controllers: [],
            services: []
          })

          resolve()
        })
      })
    })

    it('should warn the user if a invalid directory path is given to find services files in', () => {
      let kernelManager = new KernelManager(silentLogger)
      let spy = jest.spyOn(BetterConsole.prototype, 'warn')
      let directoryPath = '/path/that/doenst/exist'
      kernelManager.findServiceFilesInDirectory(directoryPath)
      
      expect(spy).toBeCalledTimes(1)
      expect(spy).toBeCalledWith(`Service path '${directoryPath}' doesn't exist.`)
    })
    
    it('should be able to find services/controllers when given a path', () => {
      let kernelManager = new KernelManager(silentLogger)
      let directoryPath = path.join(process.cwd(), 'specs', 'Helper', 'Controller')
      
      let serviceFilePaths = kernelManager.findServiceFilesInDirectory(directoryPath)
      
      expect(serviceFilePaths.length).toBe(2)
    })
    
    it('should not be able to add same named services twice', () => {
      let kernelManager = new KernelManager(silentLogger)
      let spy = jest.spyOn(BetterConsole.prototype, 'warn')
      let services = new SpecialMap<string, InifinityObject>()
      
      services = kernelManager.addService(services, DummyService, 'Dummy')
      expect(spy).toBeCalledTimes(0)
      
      services = kernelManager.addService(services, StevenHe, 'Dummy')
      expect(spy).toBeCalledTimes(1)
    })

    it('should warn if a path doesn\'t exists when fetching services from a list', () => {
      return new Promise<void>((resolve, rejects) => {
        let kernelManager = new KernelManager(silentLogger)
        let spy = jest.spyOn(BetterConsole.prototype, 'warn')
        let directoryPath = '/path/that/doenst/exist'
  
        kernelManager.fetchServicesFromList([directoryPath]).then((services) => {
          expect(spy).toBeCalledTimes(1)
          expect(services.size).toBe(0)

          resolve()
        })
      })
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('KernelManager', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}