import { shouldRunSuite } from "../../../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../../../Helper/PublicKernel";
import { MockWebserver } from "../../../Helper/Mocks/Webserver";
import { NotInitiated } from "../../../../src/2. Application/Errors/NotInitiated";
import { MissingRoute } from "../../../../src/2. Application/Errors/MissingRoute";
import { KernelManagerFactory } from "../../../Helper/PublicKernelManager";
import { MockHttpClient } from "../../../Helper/HttpClient";

if (shouldRunSuite('UNIT')) {
  let tinyKernel = new KernelMock()
  beforeAll(() => new Promise<void>((resolve, reject) => KernelFactory.generateTinyKernel(true, true, true).then((kernel) => {
    tinyKernel = kernel

    resolve()
  }).catch((e) => reject(e))))


  afterEach(() => jest.clearAllMocks())

  describe('Webserver', () => {
    it('should throw an error when getting the route container when it isn\'t initialized yet', () => {
      let webserver = new MockWebserver(tinyKernel.getLogger(), tinyKernel.getKernelManager(), tinyKernel.getContainer())

      expect(webserver.getRouteContainer.bind(webserver)).toThrowError(NotInitiated)
    })

    it('should throw an error when getting the HTTP server when it isn\'t initialized yet', () => {
      let webserver = new MockWebserver(tinyKernel.getLogger(), tinyKernel.getKernelManager(), tinyKernel.getContainer())

      expect(webserver.getServer.bind(webserver)).toThrowError(NotInitiated)
    })

    it('should not throw an error when registering middleware', () => {
      let webserver = new MockWebserver(tinyKernel.getLogger(), tinyKernel.getKernelManager(), tinyKernel.getContainer())

      expect(webserver.registerPathMiddleware.bind(webserver, '/middle', (req, res) => res.end())).not.toThrow()
    })

    it('should be able to generate a base url', () => {
      let webserver = new MockWebserver(tinyKernel.getLogger(), tinyKernel.getKernelManager(), tinyKernel.getContainer(), 8888, "127.0.0.1")
      
      expect(webserver.getBaseURL()).toBe('http://127.0.0.1:8888')
    })
    
    it('should throw an error if route is requested while no controllers are registered', () => {
      return new Promise<void>((resolve, reject) => {
        KernelFactory.generateEmptyServiceContainerKernel().then(async (kernel) => {
          kernel.createWebServer()
          let webserver = await kernel.initiateWebServer()
          
          expect(webserver.findRouteHandler.bind(webserver, '/path/does/not/exist', 'get')).toThrowError(MissingRoute)
          
          resolve()
        }).catch((e) => reject(e))
      })
    })
    
    it('should return 404 page when requesting a missing route when a controller with 404 route is registered', () => {
      return new Promise<void>((resolve, reject) => {
        let customKernelManager = KernelManagerFactory.generateTestAutoWireKernelManager(tinyKernel.getLogger())

        let webserver = new MockWebserver(tinyKernel.getLogger(), customKernelManager, tinyKernel.getContainer(), 8888, "127.0.0.1")
        webserver.initiate().then(() => {
          let client = new MockHttpClient(webserver)
  
          let [res, next] = client.get('/path/does/not/exist')
  
          expect(res.write).toBeCalledTimes(1)
          expect(res.write).toBeCalledWith('not found')
          expect(res.end).toBeCalledTimes(1)
  
          resolve()
        }).catch((e) => reject(e))
      })
    })
    
    it('should be able to fetch secure server options', () => {
      let webserver = new MockWebserver(tinyKernel.getLogger(), tinyKernel.getKernelManager(), tinyKernel.getContainer(), 8888, "127.0.0.1")
      
      expect(webserver.getSecureServerOptions()).toMatchObject({})
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Webserver', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}