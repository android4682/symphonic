import { AutoWire } from "../../src/2. Application/Decorators/Autowire";

@AutoWire()
export class AutoWireDummy
{
  @AutoWire()
  public someProperty = null

  @AutoWire()
  public someMethod(): void
  {
  }
}