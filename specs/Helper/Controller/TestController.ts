import { AutoWire } from "../../../src/2. Application/Decorators/Autowire";
import { Route } from "../../../src/2. Application/Decorators/Route";
import { Request, Response } from "express"

@AutoWire()
export class TestController
{
  @Route('/get', ['get'])
  public testRoute(req: Request, res: Response)
  {
    res.write('GETT-EGG')
    return res.end()
  }

  @Route('/post', ['post'])
  public test2Route(req: Request, res: Response)
  {
    res.write('POSTMAN TAP, POSTMAN TAP AND HIS BLACK AND WHITE CAP.')
    return res.end()
  }

  @Route('404')
  public notFoundRoute(req: Request, res: Response)
  {
    res.write('not found')
    return res.end()
  }
}
