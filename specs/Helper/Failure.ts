import { DummyService } from "./DummyService";

/**
 * @description This class is made to fail the auto wire method of Container
 */
export class StevenHe
{
  private impossibleAutoWire: DummyService;

  public constructor(impossibleAutoWire: DummyService) {
    this.impossibleAutoWire = impossibleAutoWire
  }

  public calculator(): void
  {
    throw new Error('FAILURE!')
  }
}