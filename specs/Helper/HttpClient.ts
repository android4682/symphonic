import { InifinityObject } from "@android4682/typescript-toolkit";
import { MockWebserver } from "./Mocks/Webserver";
import { getMockReq, getMockRes } from '@jest-mock/express'
import { NextFunction, Response } from "express";
import { MissingRoute } from "../../src/2. Application/Errors/MissingRoute";

export type Method = 'get' | 'post' | 'put' | 'delete' | 'patch' | 'options' | 'head'

export class MockHttpClient
{
  private webserver: MockWebserver

  public constructor(webserver: MockWebserver)
  {
    this.webserver = webserver
  }

  protected handleRequest(url: string, method: Method, query: InifinityObject = {}, body?: string | InifinityObject, contentType?: string, headers: InifinityObject = {}): [Response, NextFunction]
  {
    headers["Content-Type"] = contentType

    const req = getMockReq({
      params: (query) ? query : undefined,
      body: (body) ? body : undefined,
      headers
    })

    const { res, next } = getMockRes()
    
    let result = this.webserver.findRouteHandler(url, method)
    if (result instanceof MissingRoute) throw result
    else result[0][result[1]].call(result[0], req, res, next)

    return [res, next]
  }

  public get(url: string, query?: InifinityObject, headers?: InifinityObject): [Response, NextFunction]
  {
    return this.handleRequest(url, 'get', query, undefined, undefined, headers);
  }
  
  public post(url: string, body: string | InifinityObject, query?: InifinityObject, contentType?: string, headers?: InifinityObject): [Response, NextFunction]
  {
    return this.handleRequest(url, 'post', query, body, contentType, headers);
  }
}
