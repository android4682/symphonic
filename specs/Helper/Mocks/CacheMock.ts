import { Cache } from "../../../src/2. Application/Cache/Cache";

export class CacheMock extends Cache
{
  public connect(hostname?: string | undefined, username?: string | undefined, password?: string | undefined, database?: string | undefined, port?: number | undefined): boolean | Promise<boolean>
  {
    throw new Error("Method not implemented.");
  }
  public getOriginalCachingSystem()
  {
    throw new Error("Method not implemented.");
  }
  public get(key: any)
  {
    throw new Error("Method not implemented.");
  }
  public set(key: any, value: any, options?: any)
  {
    throw new Error("Method not implemented.");
  }
  
}