import { FixTableIntegertyLevel } from "../../../src/1. Domain/Interfaces/BasicDatabaseHandler";
import { Database } from "../../../src/2. Application/Database/Database";

export class DatabaseMock extends Database
{
  public connect(hostname?: string | undefined, username?: string | undefined, password?: string | undefined, database?: string | undefined, port?: number | undefined): boolean | Promise<boolean>
  {
    throw new Error("Method not implemented.");
  }
  public getOriginalDatabaseHandler()
  {
    throw new Error("Method not implemented.");
  }
  public checkTableIntegerty(fixLevel: FixTableIntegertyLevel): boolean | Promise<boolean>
  {
    throw new Error("Method not implemented.");
  }
}