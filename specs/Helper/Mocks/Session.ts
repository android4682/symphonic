import { Request } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { DefaultSession } from "../../../src/2. Application/Abstract/Session/DefaultSession";
import { AutoWire } from "../../../src/2. Application/Decorators/Autowire";

@AutoWire()
export class SessionMock extends DefaultSession
{
  // TODO: Expand
  public save(req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>): Promise<void>
  {
    return new Promise<void>((resolve, reject) => resolve())
  }

  // TODO: Expand
  public destroy(req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>): Promise<void>
  {
    return new Promise<void>((resolve, reject) => resolve())
  }
  
  // TODO: Expand
  public reload(req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>): Promise<void>
  {
    return new Promise<void>((resolve, reject) => resolve())
  }
}