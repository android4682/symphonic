import { Request } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { ParsedQs } from "qs";
import { Session } from "../../../src/1. Domain/Interfaces/Session";
import { DefaultSessionManager } from "../../../src/2. Application/Abstract/Manager/Session/DefaultSessionManager";

export class DefaultSessionManagerHelper extends DefaultSessionManager<Session>
{
  public async destorySession(uid: string, req: Request<ParamsDictionary, any, any, ParsedQs, Record<string, any>>, throwTantrumWhenNotFound: boolean): Promise<void>
  {
  }
}