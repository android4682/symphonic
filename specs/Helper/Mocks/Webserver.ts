import { createServer, Server } from "http";
import { Webserver } from "../../../src/3. Infrastructure/Webserver/Webserver";
import { createServer as createSecureServer, Server as SecureServer, ServerOptions as SecureServerOptions } from "https";
import { Express } from "express"
import { Method } from "../HttpClient";
import { MissingRoute } from "../../../src/2. Application/Errors/MissingRoute";
import { RouteHandler } from "../../../src/2. Application/DependencyInjection/RouteContainer";

export class MockWebserver extends Webserver
{
  public get super_router(): Express
  {
    return this.router
  }

  public findAnyRouteHandler(url: string, method: Method): RouteHandler | MissingRoute
  {
    return super.findAnyRouteHandler(url, method)
  }

  public findRouteHandler(url: string, method: Method): RouteHandler | MissingRoute
  {
    return super.findRouteHandler(url, method)
  }

  public getServer(): Server | SecureServer
  {
    return super.getServer()
  }

  protected async startWebServer(): Promise<void>
  {
    if (this.isHttps()) this.server = createSecureServer(this.getSecureServerOptions(), this.router)
    else this.server = createServer(this.getServerOptions(), this.router)
  }

  public getSecureServerOptions(): SecureServerOptions
  {
    return super.getSecureServerOptions()
  }

  public stopServer(): Promise<void>
  {
    return new Promise<void>((resolve, reject) => resolve())
  }
}