import { AutoWireConfig } from "../../src/1. Domain/Interfaces/AutoWireConfig";
import { Container } from "../../src/2. Application/DependencyInjection/Container";
import { Kernel } from "../../src/2. Application/Kernel";
import { KernelManager } from "../../src/2. Application/Manager/KernelManager";
import { BetterConsole } from "@android4682/better-console"
import { Webserver } from "../../src/3. Infrastructure/Webserver/Webserver";
import { MockWebserver } from "./Mocks/Webserver";
import { KernelManagerFactory } from "./PublicKernelManager";
import { EnvManagerMock } from "./Mocks/EnvManager";
import { EnvoirementManager } from "../../src/1. Domain/Interfaces/Env";
import { CachingSystem } from "../../src/1. Domain/Interfaces/CachingSystem";
import { BasicDatabaseHandler } from "../../src/1. Domain/Interfaces/BasicDatabaseHandler";
import { Session, SessionManager } from "../../src/1. Domain/Interfaces/Session";

export class KernelMock extends Kernel
{
  public manager: KernelManager | undefined;
  private port: number = 8080

  public getWebServerPort(): number
  {
    return this.port
  }

  public getWebServerBaseURL(): string
  {
    let webserver = <Webserver> this.getContainer().get('Webserver')
    return webserver.getBaseURL()
  }

  public getContainer(): Container
  {
    return super.getContainer()
  }
  
  public getKernelManager(): KernelManager
  {
    return super.getKernelManager()
  }
  
  public getLogger(): BetterConsole
  {
    return super.getLogger()
  }

  public getEnv(): EnvoirementManager
  {
    if (! this.env) return new EnvManagerMock()

    return this.env;
  }

  public original_getEnv(): EnvoirementManager
  {
    return super.getEnv()
  }

  private generateRandomPort(): number
  {
    return 12345
  }
  
  public initiatedEnvManager(): EnvoirementManager
  {
    return super.initiatedEnvManager();
  }

  public original_initiateEnvManager(): EnvoirementManager
  {
    return super.initiatedEnvManager();
  }

  public initiatedBetterConsole(): BetterConsole
  {
    return super.initiatedBetterConsole()
  }
  
  public initiatedKernelManager(): KernelManager
  {
    return super.initiatedKernelManager()
  }
  
  public loadAutoWireConfig(): Promise<AutoWireConfig>
  {
    return super.loadAutoWireConfig()
  }

  public loadAutoWireableServices(): Promise<Container>
  {
    return super.loadAutoWireableServices()
  }
  
  public original_createWebServer(): Webserver
  {
    return super.createWebServer()
  }
  
  public original_initiateWebServer(): Promise<Webserver>
  {
    return super.initiateWebServer()
  }

  public initiateCaching(): Promise<CachingSystem>
  {
    return super.initiateCaching()
  }

  public initiateDatabase(): Promise<BasicDatabaseHandler>
  {
    return super.initiateDatabase()
  }

  public initiateSessionManager(): SessionManager<Session>
  {
    return super.initiateSessionManager()
  }

  public createWebServer(): MockWebserver
  {
    this.getLogger().debug("Creating Mock web server...")

    let webserver = <MockWebserver> this.getContainer().autoWireService(MockWebserver, [], true)

    this.getContainer().registerService(webserver, "Webserver")

    return webserver
  }

  public async initiateWebServer(): Promise<MockWebserver>
  {
    this.getLogger().debug("Initiating Mock web server...")

    let webserver = <MockWebserver> this.getContainer().get('Webserver')

    await webserver.initiate()

    return webserver
  }
}

export class KernelFactory
{
  private static normalKernel: KernelMock | undefined = undefined
  private static tinyKernel: KernelMock | undefined = undefined
  private static emptyServiceContainerKernel: KernelMock | undefined = undefined

  public static async generateKernel(freshKernel: boolean = true): Promise<KernelMock>
  {
    if (this.normalKernel && ! freshKernel) {
      return this.normalKernel
    }

    let kernel = new KernelMock()
    await kernel.init()
    
    return this.normalKernel = kernel
  }

  public static async generateKernelWithTestEnv(): Promise<KernelMock>
  {
    let kernel = new KernelMock()

    kernel.initiatedEnvManager()
    kernel.initiatedBetterConsole()
    kernel.manager = KernelManagerFactory.generateTestAutoWireKernelManager(kernel.getLogger())
    await kernel.loadAutoWireConfig()
    await kernel.loadAutoWireableServices()
    await kernel.initiateCaching()
    await kernel.initiateDatabase()
    kernel.initiateSessionManager()
    kernel.createWebServer()
    await kernel.initiateWebServer()
    
    return kernel
  }
  
  public static async generateTinyKernel(freshKernel: boolean = true, includeKernelManager: boolean = false, alsoIncludeAutoWireConfigAndContainer: boolean = false): Promise<KernelMock>
  {
    if (this.tinyKernel && ! freshKernel) {
      return this.tinyKernel
    }

    let kernel = new KernelMock()

    kernel.initiatedEnvManager()
    kernel.initiatedBetterConsole()
    if (includeKernelManager) {
      kernel.initiatedKernelManager()
      if (alsoIncludeAutoWireConfigAndContainer) {
        await kernel.loadAutoWireConfig()
        await kernel.loadAutoWireableServices()
      }
    }
    
    return this.tinyKernel = kernel
  }
  
  public static async generateEmptyServiceContainerKernel(freshKernel: boolean = true): Promise<KernelMock>
  {
    if (this.emptyServiceContainerKernel && ! freshKernel) {
      return this.emptyServiceContainerKernel
    }
    
    let kernel = new KernelMock()
    
    kernel.initiatedBetterConsole()
    kernel.manager = KernelManagerFactory.generateNoAutoWireKernelManager(kernel.getLogger())
    await kernel.loadAutoWireConfig()
    await kernel.loadAutoWireableServices()

    return this.emptyServiceContainerKernel = kernel
  }
}
