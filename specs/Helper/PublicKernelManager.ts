import { BetterConsole } from "@android4682/better-console";
import { KernelManager } from "../../src/2. Application/Manager/KernelManager";
import { AutoWireConfig } from "../../src/1. Domain/Interfaces/AutoWireConfig";
import path from "path";

export class PublicKernelManager extends KernelManager
{
  public allowAutoWire: boolean = true
  public autoWireIsTest: boolean = false

  public get autoWireConfig_empty(): AutoWireConfig
  {
    return {
      services: [],
      controllers: []
    }
  }

  public get autoWireConfig_test(): AutoWireConfig
  {
    return {
      services: [],
      controllers: [
        path.join(process.cwd(), 'specs', 'Helper', 'Controller', 'TestController.ts')
      ]
    }
  }

  public getAutoWireConfig(): AutoWireConfig
  {
    if (! this.allowAutoWire) return this.autoWireConfig_empty
    if (this.autoWireIsTest) return this.autoWireConfig_test

    return super.getAutoWireConfig()
  }

  public getLiveAutoWireConfig(): Promise<AutoWireConfig>
  {
    return new Promise<AutoWireConfig>((resolve, reject) => {
      if (! this.allowAutoWire) return resolve(this.autoWireConfig_empty)
      if (this.autoWireIsTest) return resolve(this.autoWireConfig_test)
  
      super.getLiveAutoWireConfig().then((config) => resolve(config)).catch((e) => reject(e))
    })
  }
}

export class KernelManagerFactory
{
  public static generateKernelManager(customLogger: BetterConsole = new BetterConsole()): PublicKernelManager
  {
    return new PublicKernelManager(customLogger)
  }

  public static generateNoAutoWireKernelManager(customLogger: BetterConsole = new BetterConsole()): PublicKernelManager
  {
    let kernelManager = this.generateKernelManager(customLogger)

    kernelManager.allowAutoWire = false

    return kernelManager
  }

  public static generateTestAutoWireKernelManager(customLogger: BetterConsole = new BetterConsole()): PublicKernelManager
  {
    let kernelManager = this.generateKernelManager(customLogger)

    kernelManager.autoWireIsTest = true

    return kernelManager
  }
}