import { Route } from "../../src/2. Application/Decorators/Route";

export class RouteDummy
{
  @Route('*')
  public catchAllRoute(): void
  {
  }
}