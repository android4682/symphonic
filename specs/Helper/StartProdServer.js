const { Kernel } = require('../../dist/index')

let kernel = new Kernel();
if (process.argv.indexOf('--compile-validation') !== -1) kernel.initTest()
else kernel.init()
