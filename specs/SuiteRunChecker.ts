const testArgument: TestArgumentType = <TestArgumentType> process.env['TESTS'] || 'ALL'

export type SuiteType = 'UNIT' | 'FUNCTIONAL'
export type TestArgumentType = SuiteType | 'ALL'

function isArgumentPassed(argument: TestArgumentType): boolean
{
  return (testArgument === argument)
}

export function shouldRunSuite(suiteType: SuiteType): boolean
{
  return (isArgumentPassed("ALL") || isArgumentPassed(suiteType))
}
