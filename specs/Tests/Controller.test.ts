import { shouldRunSuite } from "../SuiteRunChecker";
import { KernelFactory, KernelMock } from "../Helper/PublicKernel";
import { TestController } from "../Helper/Controller/TestController";
import { MockHttpClient } from "../Helper/HttpClient";
import { MockWebserver } from "../Helper/Mocks/Webserver";

if (shouldRunSuite('FUNCTIONAL')) {
  let kernel: KernelMock = new KernelMock()

  beforeAll(() => new Promise<void>((resolve, reject) => {
    KernelFactory.generateKernelWithTestEnv().then((publicKernel) => {
      kernel = publicKernel
      
      resolve()
    }).catch((e) => reject(e))
  }));
  
  describe('TestController', () => {
    it('should be able to be initiated through kernel', () => {
      let webserver = <MockWebserver> kernel.getContainer().get('Webserver')
      let routeContainer = webserver.getRouteContainer()
      let testController: TestController | undefined = undefined
      
      try {
        testController = <TestController> routeContainer.get(TestController.name)
      } catch(e) {}
      
      expect(testController).not.toBe(undefined)
    })
    
    it('should have a GET endpoint', () => {
      let webserver = <MockWebserver> kernel.getContainer().get('Webserver')

      let client = new MockHttpClient(webserver)
      let [res, next] = client.get('/get')

      expect(res.write).toHaveBeenCalledWith(expect.stringMatching('GETT-EGG'))
      expect(res.end).toHaveBeenCalled()
      expect(next).not.toHaveBeenCalled()
    })

    it('should have a POST endpoint', () => {
      let webserver = <MockWebserver> kernel.getContainer().get('Webserver')

      let client = new MockHttpClient(webserver)
      let [res, next] = client.post('/post', '')

      expect(res.write).toHaveBeenCalledWith(expect.stringMatching('POSTMAN TAP, POSTMAN TAP AND HIS BLACK AND WHITE CAP.'))
      expect(res.end).toHaveBeenCalled()
      expect(next).not.toHaveBeenCalled()
    })
  })
}

if (shouldRunSuite('UNIT')) {
  if (! shouldRunSuite('FUNCTIONAL')) {
    describe('TestController', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })

    describe('ExpressSessionManager', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}
