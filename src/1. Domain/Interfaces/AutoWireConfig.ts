export type AutoWireConfig = {
  services: string[]
  controllers: string[]
}
