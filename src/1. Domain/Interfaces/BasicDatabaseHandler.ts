export enum FixTableIntegertyLevel
{
  'none',
  'safe',
  'force'
}

export interface BasicDatabaseHandler
{
  connect(hostname?: string, username?: string, password?: string, database?: string, port?: number): Promise<boolean> | boolean
  getOriginalDatabaseHandler(): any
  checkTableIntegerty(fixLevel: FixTableIntegertyLevel): Promise<boolean> | boolean
}
