export interface CachingSystem
{
  connect(hostname?: string, username?: string, password?: string, database?: string, port?: number): Promise<boolean> | boolean
  getOriginalCachingSystem(): any
  get(key: any): Promise<any> | any
  set(key: any, value: any, options?: any): Promise<any> | any
}
