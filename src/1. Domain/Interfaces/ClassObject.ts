export interface ClassObject<T>
{
  new (...args: any[]): T
}
