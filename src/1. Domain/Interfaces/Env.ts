import { LogLevel } from "@android4682/better-console";
import { FixTableIntegertyLevel } from "./BasicDatabaseHandler";

export interface Env
{
  [key: string]: string | undefined
  loglevel: string | undefined
  autowire_config_filepath: string | undefined
  port: string | undefined
  hostname: string | undefined
  database_driver: string | undefined
  database_hostname: string | undefined
  database_port: string | undefined
  database_name: string | undefined
  database_username: string | undefined
  database_password: string | undefined
  cache_driver: string | undefined
  cache_hostname: string | undefined
  cache_port: string | undefined
  cache_database: string | undefined
  cache_username: string | undefined
  cache_password: string | undefined
  session_provider: string | undefined
  session_name: string | undefined
  session_resave: string | undefined
  session_save_uninitialized: string | undefined
  session_random_secret_on_bootup: string | undefined
  session_unset_policy: string | undefined
  session_secure: string | undefined
  session_max_age: string | undefined
  session_http_only: string | undefined
}

export interface ParsedEnv
{
  loglevel: LogLevel
  autowire_config_filepath: string | undefined
  port: number
  hostname: string
  database_driver: string
  database_hostname: string | undefined
  database_port: number | undefined
  database_name: string | undefined
  database_username: string | undefined
  database_password: string | undefined
  database_fix_table_integerty: FixTableIntegertyLevel
  cache_driver: string
  cache_hostname: string | undefined
  cache_port: number | undefined
  cache_database: string | undefined
  cache_username: string | undefined
  cache_password: string | undefined
  session_provider: string
  session_name: string
  session_resave: boolean
  session_save_uninitialized: boolean
  session_random_secret_on_bootup: boolean
  session_unset_policy: 'destroy' | 'keep'
  session_secure: boolean
  session_max_age: number
  session_http_only: boolean
}

export interface EnvoirementManager extends ParsedEnv
{
  Env: Env
}