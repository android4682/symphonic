export interface HttpCredentials extends Object
{
  key?: string,
  cert?: string
}