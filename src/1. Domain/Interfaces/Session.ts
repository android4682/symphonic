import { InifinityObject } from "@android4682/typescript-toolkit";
import { Request } from "express";
import { Session as OriginalExpressSession } from "express-session"

export type SessionDataType = InifinityObject

export interface StaticSession<S extends Session> extends Session
{
  new (...args: any[]): S
}

export interface Session
{
  uid: string
  session: SessionDataType
  getRawSession(req: Request): RawSession
  save(req: Request): Promise<void>
  destroy(req: Request): Promise<void>
  reload(req: Request): Promise<void>
}

export interface SessionManager<S extends Session>
{
  sessionClassName: string
  createSession(sessionData: SessionDataType, forceUID: string): S;
  addSession(session: S, ...args:[]): S;
  updateSession(uid: string, sessionData: SessionDataType, createIfAbsence: boolean): S | false;
  findSessionByUID(uid: string): S | null;
  destorySession(uid: string, req: Request, throwTantrumWhenNotFound: boolean): Promise<void>;
}

export interface RawSession extends OriginalExpressSession
{
  data?: InifinityObject
}