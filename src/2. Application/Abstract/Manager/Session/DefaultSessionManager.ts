import { InifinityObject } from "@android4682/typescript-toolkit";
import { Request } from "express";
import { Session, SessionManager, StaticSession } from "../../../../1. Domain/Interfaces/Session";
import { Container } from "../../../DependencyInjection/Container";
import { AutoWire } from "../../../Decorators/Autowire";

@AutoWire()
export abstract class DefaultSessionManager<S extends Session> implements SessionManager<S>
{
  protected sessions: S[] = []
  protected staticClass: StaticSession<S>

  public constructor(container: Container, sessionName: string = 'session')
  {
    let session = <Session> container.get(sessionName)
    
    this.staticClass = <StaticSession<S>> session.constructor
  }

  public get sessionClassName(): string
  {
    return 'Session'
  }
  
  public createSession(sessionData: InifinityObject = {}, forceUID?: string): S
  {
    let session = <S> new this.staticClass(sessionData, forceUID)

    return this.addSession(session)
  }

  public addSession(session: S): S
  {
    this.sessions.push(session)

    return session
  }

  public updateSession(uid: string, sessionData: InifinityObject, createIfAbsence: boolean = false): S | false
  {
    const session = this.findSessionByUID(uid)
    if (session === null) {
      if (createIfAbsence) return this.createSession(sessionData, uid)
      else return false
    } else session.session = sessionData

    return session
  }

  public findSessionByUID(uid: string): S | null
  {
    for (let i = 0; i < this.sessions.length; i++) {
      const session = this.sessions[i];
      if (session.uid === uid) return session
    }

    return null
  }

  public abstract destorySession(uid: string, req: Request, throwTantrumWhenNotFound: boolean): Promise<void>
}
