import { InifinityObject, randomStringGenerator } from "@android4682/typescript-toolkit";
import { Request } from "express";
import { RawSession, Session } from "../../../1. Domain/Interfaces/Session";
import "express-session";

export abstract class DefaultSession implements Session
{
  protected _uid: string
  protected _session: InifinityObject

  public constructor(sessionData: InifinityObject = {}, forceUID: string = randomStringGenerator(64))
  {
    this._uid = forceUID
    this._session = sessionData
  }

  public get uid(): string
  {
    return this._uid
  }

  public get session(): InifinityObject
  {
    return this._session
  }

  public set session(value: InifinityObject)
  {
    this._session = value
  }

  public getRawSession(req: Request): RawSession
  {
    return req.session
  }

  public abstract save(req: Request): Promise<void>;

  public abstract destroy(req: Request): Promise<void>;

  public abstract reload(req: Request): Promise<void>;
}