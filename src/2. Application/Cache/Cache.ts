import { CachingSystem } from "../../1. Domain/Interfaces/CachingSystem";

export abstract class Cache implements CachingSystem
{
  public abstract connect(hostname?: string | undefined, username?: string | undefined, password?: string | undefined, database?: string | undefined, port?: number | undefined): boolean | Promise<boolean>
  public abstract getOriginalCachingSystem(): any
  public abstract get(key: any): any
  public abstract set(key: any, value: any, options?: any): any
}