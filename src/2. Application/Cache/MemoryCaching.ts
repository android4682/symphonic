import { InifinityObject } from "@android4682/typescript-toolkit";
import { CachingSystem } from "../../1. Domain/Interfaces/CachingSystem";

export class SymphonicMemoryCaching implements CachingSystem
{
  protected cache: InifinityObject = {}

  public connect(url?: string, username?: string, password?: string, database?: string, port?: number): boolean
  {
    return true;
  }

  public getOriginalCachingSystem(): InifinityObject
  {
    return this.cache
  }

  public get(key: keyof InifinityObject): any
  {
    return this.cache[key]
  }

  public set(key: string | number, value: any, options?: any): void
  {
    this.cache[key] = value
  }
}