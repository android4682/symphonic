export const METADATA_ROUTE = 'custom:route'
export const METADATA_METHODS = 'custom:methods'
export const METADATA_AUTOWIRE = 'custom:shouldAutowire'