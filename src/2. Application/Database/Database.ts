import { BasicDatabaseHandler, FixTableIntegertyLevel } from "../../1. Domain/Interfaces/BasicDatabaseHandler";

export abstract class Database implements BasicDatabaseHandler
{
  public abstract connect(hostname?: string | undefined, username?: string | undefined, password?: string | undefined, database?: string | undefined, port?: number | undefined): boolean | Promise<boolean>
  public abstract getOriginalDatabaseHandler(): any
  public abstract checkTableIntegerty(fixLevel: FixTableIntegertyLevel): boolean | Promise<boolean>
}