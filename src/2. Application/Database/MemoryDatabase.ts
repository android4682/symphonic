import { InifinityObject } from "@android4682/typescript-toolkit";
import { BasicDatabaseHandler, FixTableIntegertyLevel } from "../../1. Domain/Interfaces/BasicDatabaseHandler";

export class SymphonicMemoryDatabase implements BasicDatabaseHandler
{
  protected database: InifinityObject = {}

  public connect(url?: string | undefined, username?: string | undefined, password?: string | undefined, database?: string | undefined, port?: number | undefined): boolean
  {
    return true;
  }

  public getOriginalDatabaseHandler()
  {
    return this.database;
  }

  public checkTableIntegerty(fixLevel: FixTableIntegertyLevel): boolean
  {
    return true;
  }
}