import { Method } from "../../1. Domain/Interfaces/Webserver";
import { METADATA_METHODS, METADATA_ROUTE } from "../Constants";

export function Route(path: string, method: Method[] = ['all']): Function
{
  return (target: object, propertyKey: string | symbol) => {
    Reflect.defineMetadata(METADATA_ROUTE, path, target, propertyKey);
    Reflect.defineMetadata(METADATA_METHODS, method, target, propertyKey);
  }
}
