import "reflect-metadata";
import { InifinityObject, SpecialMap, isClass, isDeepInstanceOf } from "@android4682/typescript-toolkit";
import { MissingService } from "../Errors/MissingService";
import { ClassObject } from "../../1. Domain/Interfaces/ClassObject";
import { BetterConsole, getMessageAndStack } from "@android4682/better-console";
import { FailedToAutowire } from "../Errors/FailedToAutowire";
import { METADATA_AUTOWIRE } from "../Constants";

export class Container
{
  protected services: SpecialMap<string, InifinityObject> = new SpecialMap<string, InifinityObject>()
  protected logger: BetterConsole;

  public constructor(logger: BetterConsole, services: SpecialMap<string, InifinityObject> = new SpecialMap<string, InifinityObject>())
  {
    this.logger = logger
    services.set(this.constructor.name, this)
    this.services = services
    this.constructServices(this.services)
  }

  /**
   * @throws { MissingService }
   */
  public get(serviceName: string, throwTantrumWhenMissing: boolean = true): object | false
  {
    let service = this.services.get(serviceName)

    if (! service) {
      if (throwTantrumWhenMissing) throw new MissingService(serviceName);
      else return false
    }

    return service
  }

  public registerService(service: ClassObject<any> | InifinityObject, serviceName: string, ignoreMissingParamters: boolean = false, overwrite: boolean = false): boolean
  {
    if (this.services.has(serviceName)) {
      if (overwrite) {
        this.logger.warn(`Overwriting registered service '${serviceName}'...`)
      } else {
        this.logger.warn(`Service '${serviceName}' is already registered.`)
  
        return false;
      }
    }

    if (this.isServiceStatic(service)) {
      try {
        service = this.autoWireService(<ClassObject<any>> service, [], ignoreMissingParamters)
      } catch (e) {
        let [msg, stack] = getMessageAndStack(e)
        this.logger.error(msg)
        this.logger.dir(stack)

        return false
      }
    }

    this.services.set(serviceName, service)

    return true;
  }

  public isServiceStatic(service: ClassObject<any> | InifinityObject): boolean
  {
    return (!! service.prototype && !! service.prototype.constructor)
  }

  /**
   * @throws { FailedToAutowire }
   */
  public getParameterTypes(objectPrototype: any, methodName: string): any[]
  {
    let parameters: any[] | undefined = undefined
    let isAutoWireable = false

    if (methodName === 'constructor') {
      isAutoWireable = Reflect.getMetadata(METADATA_AUTOWIRE, objectPrototype)
      parameters = Reflect.getMetadata("design:paramtypes", objectPrototype)
    } else {
      isAutoWireable = Reflect.getMetadata(METADATA_AUTOWIRE, objectPrototype, methodName)
      parameters = Reflect.getMetadata("design:paramtypes", objectPrototype, methodName);
    }
    
    if (parameters === undefined) {
      if (isAutoWireable === true) parameters = []
      else throw new FailedToAutowire(objectPrototype.name, methodName)
    }

    return parameters
  }

  private canSkipAutoWiring(arg: any, parameterType: any): boolean
  {
    return (arg !== undefined || parameterType === undefined || (arg !== undefined && isDeepInstanceOf(arg, parameterType)))
  }

  /**
   * @throws { FailedToAutowire }
   * @throws { MissingService }
   */
  public autoWireService(classObject: ClassObject<any>, args: any[] = [], ignoreMissingParamters: boolean = false): any
  {
    let parameterTypes = this.getParameterTypes(classObject, 'constructor')

    for (let i = 0; i < parameterTypes.length; i++) {
      const parameterType = parameterTypes[i];
      
      if (this.canSkipAutoWiring(args[i], parameterType)) continue

      try {
        args[i] = this.get(parameterType.name)
      } catch (e) {
        if (! ignoreMissingParamters) throw e 
      }
      
      if (args[i] && isClass(args[i])) {
        args[i] = this.autoWireService(args[i], [], ignoreMissingParamters)
      }
    }

    return new classObject(...args)
  }

  public constructServices(services: SpecialMap<string, ClassObject<any> | InifinityObject>): typeof services
  {
    let initiated: string[] = []
    let lastRound: number = 0
    
    do {
      lastRound = initiated.length

      services.forEach((service, name) => {
        if (! this.isServiceStatic(service)) {
          if (initiated.indexOf(name) === -1) {
            initiated.push(name)
          }
          
          return
        }
  
        let initiatedService = this.autoWireService(<ClassObject<any>> service, [], true)
        
        if (! this.isServiceStatic(initiatedService)) {
          initiated.push(name)

          services.set(name, initiatedService)
        }
      })
    } while (initiated.length < services.keysAsArray().length && lastRound !== initiated.length)

    if (initiated.length < services.keysAsArray().length) {
      // TODO: Delete missing services!
      let missingServices: string[] = []

      for (let i = 0; i < services.keysAsArray().length; i++) {
        const serviceName = services.keysAsArray()[i];
        if (initiated.indexOf(serviceName) === -1) missingServices.push(serviceName)
      }

      this.logger.warn('The following services didn\'t get constructed, probably because of missing parameters which are missing in the dependency injector container.')
      this.logger.warn(missingServices)
    }

    return services
  }

  /**
   * @returns { boolean } False if any failed, true if all succeded
   */
  public regiserServiceList(services: SpecialMap<string, InifinityObject>, ignoreMissingParamters: boolean = false, overwrite: boolean = false): boolean
  {
    let totalSuccess = true
    services.forEach((service, name) => {
      let success = this.registerService(service, name, ignoreMissingParamters, overwrite)
      if (totalSuccess && ! success) totalSuccess = success
    })

    return totalSuccess
  }

  public getServicesThatAreAnInstanceOf(instance: any): SpecialMap<string, any>
  {
    let list = new SpecialMap<string, any>()

    this.services.forEach((value, key) => {
      if (isDeepInstanceOf(value, instance)) {
        list.set(key, value)
      }
    })

    return list
  }
}
