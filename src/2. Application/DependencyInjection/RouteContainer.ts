import { RequestHandler } from "express";
import { Method } from "../../1. Domain/Interfaces/Webserver";
import { METADATA_METHODS, METADATA_ROUTE } from "../Constants";
import { Container } from "./Container";
import { MissingRoute } from "../Errors/MissingRoute";
import { InifinityObject, SpecialMap } from "@android4682/typescript-toolkit";
import { ClassObject } from "../../1. Domain/Interfaces/ClassObject";
import { BetterConsole } from "@android4682/better-console";

export type RouteHandler = [InifinityObject, string]

export class RouteContainer extends Container
{
  private container: Container;

  public constructor(logger: BetterConsole, serviceContainer: Container, routeControllers: SpecialMap<string, ClassObject<any>> = new SpecialMap<string, ClassObject<any>>())
  {
    super(logger)
    this.container = serviceContainer

    this.services = new SpecialMap<string, ClassObject<any>>()
    routeControllers.forEach((staticController, name) => {
      this.logger.dev('Registering route controllers...')
      let service = this.container.autoWireService(staticController)
      this.services.set(name, service)
    })

    this.services = this.container.constructServices(this.services)
  }

  /**
   * @throws { MissingRoute } If the given route and method combination has not been found.
   */
  public findRouteHandler(path: string, method?: Method, throwTantrumWhenMissing: boolean = true): RouteHandler | MissingRoute
  {
    let handler: RouteHandler | undefined = undefined 

    this.services.forEach((controller, name) => {
      if (!! handler && handler.length > 0) {
        return;
      }

      for (let i = 0; i < Object.getOwnPropertyNames(controller.constructor.prototype).length; i++) {
        const childName = Object.getOwnPropertyNames(controller.constructor.prototype)[i];
        if (childName === 'constructor') continue

        let childRoute = Reflect.getMetadata(METADATA_ROUTE, controller, childName)
        let childMethod = Reflect.getMetadata(METADATA_METHODS, controller, childName)

        if (childRoute === path && (! method || childMethod.indexOf(method) !== -1)) {
          handler = [controller, childName]
        }
      }
    })

    if (! handler) {
      let error = new MissingRoute(path, method || 'all');
      if (throwTantrumWhenMissing) throw error
      else return error
    }

    return handler
  }
}