export class FailedToAutowire extends Error {
  public errorName: string = this.constructor.name;

  public constructor(className: string, methodName: string)
  {
    super(`Couldn't autowire '${className}.${methodName}'. Did you add '@AutoWire' to it?`)
  }
}
