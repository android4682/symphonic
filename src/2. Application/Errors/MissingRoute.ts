import { Method } from "../../1. Domain/Interfaces/Webserver";

export class MissingRoute extends Error {
  public errorName: string = this.constructor.name;

  public constructor(route: string, method: Method)
  {
    super(`Couldn't find route ${method.toUpperCase()} '${route}'.`)
  }
}