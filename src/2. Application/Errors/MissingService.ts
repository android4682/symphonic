export class MissingService extends Error {
  public errorName: string = this.constructor.name;

  public constructor(serviceName: string)
  {
    super(`Couldn't find service with name '${serviceName}', did you configure it?`)
  }
}