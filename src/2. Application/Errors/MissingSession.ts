export class MissingSession extends Error {
  public errorName: string = this.constructor.name;

  public constructor(uid: string)
  {
    super(`Couldn't find session by uid '${uid}'.`)
  }
}
