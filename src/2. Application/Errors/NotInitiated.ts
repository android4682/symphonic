export class NotInitiated extends Error {
  public errorName: string = this.constructor.name;

  public constructor(name: string)
  {
    super(`A resource in '${name}' was accessed that wasn't initiated.`)
  }
}