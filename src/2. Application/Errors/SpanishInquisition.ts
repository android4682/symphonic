export class SpanishInquisition extends Error
{
  public errorName: string = this.constructor.name;

  public constructor()
  {
    super(`NOBODY EXPECTS THE SPANISH INQUISITION!`)
  }
}