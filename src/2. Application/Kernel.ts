import { BetterConsole, LogLevel } from "@android4682/better-console";
import fs from "node:fs";
import { InifinityObject, SpecialMap } from "@android4682/typescript-toolkit";
import { Container } from "./DependencyInjection/Container";
import { Webserver } from "../3. Infrastructure/Webserver/Webserver";
import { KernelManager } from "./Manager/KernelManager";
import { NotInitiated } from "./Errors/NotInitiated";
import { AutoWireConfig } from "../1. Domain/Interfaces/AutoWireConfig";
import { HttpCredentials } from "../1. Domain/Interfaces/HttpCredentials";
import { BasicDatabaseHandler, FixTableIntegertyLevel } from "../1. Domain/Interfaces/BasicDatabaseHandler";
import { EnvManager } from "./Manager/EnvManager";
import { EnvoirementManager } from "../1. Domain/Interfaces/Env";
import { SymphonicMemoryCaching } from "./Cache/MemoryCaching";
import { SymphonicMemoryDatabase } from "./Database/MemoryDatabase";
import { CachingSystem } from "../1. Domain/Interfaces/CachingSystem";
import { Session, SessionManager } from "../1. Domain/Interfaces/Session";
import { ExpressSessionManager } from "./Manager/Session/ExpressSessionManager";
import { ExpressSession } from "./Session/ExpressSession";

export class Kernel
{
  protected container: Container | undefined
  protected manager: KernelManager | undefined
  protected logger: BetterConsole | undefined
  protected env: EnvoirementManager | undefined

  protected getContainer(): Container
  {
    if (! this.container) throw new NotInitiated('Kernel.container')

    return this.container
  }

  protected getKernelManager(): KernelManager
  {
    if (! this.manager) throw new NotInitiated('Kernel.manager')

    return this.manager
  }

  protected getLogger(): BetterConsole
  {
    if (! this.logger) throw new NotInitiated('Kernel.logger')

    return this.logger
  }

  protected getEnv(): EnvoirementManager
  {
    if (! this.env) throw new NotInitiated('Kernel.env')

    return this.env
  }

  protected initiatedEnvManager(): EnvoirementManager
  {
    return this.env = new EnvManager()
  }

  protected initiatedBetterConsole(): BetterConsole
  {
    let loglevel = this.getEnv().loglevel

    return this.logger = new BetterConsole(loglevel)
  }

  protected initiatedKernelManager(): KernelManager
  {
    this.getLogger().debug("Initiating kernel manager...")
    return this.manager = new KernelManager(this.getLogger())
  }

  protected async loadAutoWireConfig(): Promise<AutoWireConfig>
  {
    this.getLogger().debug("Loading auto wire config...")
    return await this.getKernelManager().getLiveAutoWireConfig()
  }

  protected async loadAutoWireableServices(): Promise<Container>
  {
    this.getLogger().debug("Loading auto wireable services...")
    let autoWireConfig = this.getKernelManager().getAutoWireConfig()
    
    let services = new SpecialMap<string, InifinityObject>()
    
    // Pre-container
    const logger = this.getLogger()
    const kernelManager = this.getKernelManager()
    const env = this.getEnv()
    // If the names changes scripts might not like it. So also keep the original name.
    services = this.getKernelManager().addService(services, logger, BetterConsole.name)
    services = this.getKernelManager().addService(services, kernelManager, KernelManager.name)
    services = this.getKernelManager().addService(services, env, EnvManager.name)
    if (logger.constructor.name !== BetterConsole.name) services = this.getKernelManager().addService(services, logger, logger.constructor.name)
    if (kernelManager.constructor.name !== KernelManager.name) services = this.getKernelManager().addService(services, kernelManager, kernelManager.constructor.name)
    services = this.getKernelManager().addService(services, env, env.constructor.name)
    services = this.getKernelManager().addService(services, new SymphonicMemoryDatabase(), SymphonicMemoryDatabase.name)
    services = this.getKernelManager().addService(services, new SymphonicMemoryCaching(), SymphonicMemoryCaching.name)
    
    this.container = new Container(this.getLogger(), services)
    
    // Post-container
    this.container.registerService(new ExpressSession({}, 'DEFAULT'), ExpressSession.name, true)
    this.container.registerService(ExpressSessionManager, ExpressSessionManager.name, false)
    
    // Post-container - User services
    let userServices = await this.getKernelManager().fetchServicesFromList(autoWireConfig.services)
    this.container.regiserServiceList(userServices)

    return this.container
  }
  
  protected async initiateDatabase(): Promise<BasicDatabaseHandler>
  {
    let env = this.getEnv()
    let databaseDriver: string = env.database_driver

    this.getLogger().debug("Initiating database...")
    
    let databaseHandler = <BasicDatabaseHandler> this.getContainer().get(databaseDriver)
    await databaseHandler.connect(env.database_hostname, env.database_username, env.database_password, env.database_name, env.database_port)
    
    this.getLogger().debug("Checking database table integerty...")
    if (! await databaseHandler.checkTableIntegerty(env.database_fix_table_integerty)) {
      let msg = 'Database table intergerty is incorrect'
      switch (env.database_fix_table_integerty) {
        case FixTableIntegertyLevel.force:
          msg += ' and could not be fixed'
          break;
        case FixTableIntegertyLevel.safe:
          msg += ', this might be fixable by replace \'--fix-db\' with \'--force-fix-db\' but, WARNING, this might result in a loss of data'
          break;
        case FixTableIntegertyLevel.none:
          msg += ', if you want to auto fix this add \'--fix-db\' to the start command. This will try to auto fix the database integerty issues in a safe manner (no data loss)'
          break;
      }
      msg += '. See the above error for more information.'
      throw new Error(msg)
    }

    this.getContainer().registerService(databaseHandler, 'Database', false, true)
    
    return databaseHandler
  }
  
  protected async initiateCaching(): Promise<CachingSystem>
  {
    let env = this.getEnv()
    let cacheDriver: string = env.cache_driver

    this.getLogger().debug("Initiating caching...")
    
    let cacheHandler = <CachingSystem> this.getContainer().get(cacheDriver)
    await cacheHandler.connect(env.cache_hostname, env.cache_username, env.cache_password, env.cache_database, env.cache_port)
    
    this.getContainer().registerService(cacheHandler, 'Cache', false, true)
    
    return cacheHandler
  }

  protected initiateSessionManager(): SessionManager<Session>
  {
    let env = this.getEnv()

    let sessionManager = <SessionManager<Session>> this.getContainer().get(env.session_provider)
    this.getContainer().registerService(sessionManager, 'sessionManager')

    let session = <Session> this.getContainer().get(sessionManager.sessionClassName)
    this.getContainer().registerService(session, 'session')

    return sessionManager
  }
  
  protected createWebServer(): Webserver
  {
    this.getLogger().debug("Creating web server...")
    let env = this.getEnv()

    let keyFile = undefined
    if (process.env.keyFile) {
      let keyFilePath = process.env.keyFile.replace('{{CWD}}', process.cwd())
      if (fs.existsSync(keyFilePath)) keyFile = keyFilePath
    }
    let certFile = undefined
    if (process.env.certFile) {
      let certFilePath = process.env.certFile.replace('{{CWD}}', process.cwd())
      if (fs.existsSync(certFilePath)) certFile = certFilePath
    }

    let httpsCredentials: HttpCredentials = {}
    if (!! keyFile && !! certFile) {
      httpsCredentials = {
        key: fs.readFileSync(keyFile).toString(),
        cert: fs.readFileSync(certFile).toString()
      }
    }

    let webserver = <Webserver> this.getContainer().autoWireService(Webserver, [undefined, undefined, undefined, env.port, env.hostname, httpsCredentials], true)

    this.getContainer().registerService(webserver, "Webserver", false, true)

    return webserver
  }
  
  protected async initiateWebServer(): Promise<Webserver>
  {
    this.getLogger().debug("Initiating web server...")
    let webserver = <Webserver> this.getContainer().get('Webserver')

    await webserver.initiate()

    return webserver
  }

  public async init(): Promise<void>
  {
    this.initiatedEnvManager()
    this.initiatedBetterConsole()
    this.getLogger().info("Booting up...")
    this.initiatedKernelManager()
    await this.loadAutoWireConfig()
    await this.loadAutoWireableServices()
    await this.initiateCaching()
    await this.initiateDatabase()
    this.initiateSessionManager()
    this.createWebServer()
    await this.initiateWebServer()

    this.getLogger().dir({container: this.getContainer()}, undefined, LogLevel.dev)
  }

  public initTest(exit: boolean = true): void
  {
    console.log('Script can run. Test complete.')

    if (exit) process.exit(0)
  }
}
