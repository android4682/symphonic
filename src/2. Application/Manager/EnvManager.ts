import { LogLevel } from "@android4682/better-console";
import { Env, EnvoirementManager } from "../../1. Domain/Interfaces/Env";
import dotenv from "dotenv";
import path from "node:path";
import { FixTableIntegertyLevel } from "../../1. Domain/Interfaces/BasicDatabaseHandler";
import { SymphonicMemoryCaching } from "../Cache/MemoryCaching";
import { SymphonicMemoryDatabase } from "../Database/MemoryDatabase";
import os from "node:os";
import { autoConvertString } from "@android4682/typescript-toolkit";

export class EnvManager implements EnvoirementManager
{
  public constructor()
  {
    this.initiate()
  }
  
  protected initiate(): void
  {
    dotenv.config({
      "path": path.join(process.cwd(), ".env")
    })
  }

  public get Env(): Env
  {
    let env = <unknown> process.env
    return <Env> env
  }

  public get loglevel(): LogLevel
  {
    return <LogLevel> parseInt(this.Env.loglevel || '3')
  }

  public get autowire_config_filepath(): string | undefined
  {
    return this.Env.autowire_config_filepath;
  }

  public get port(): number
  {
    return parseInt(this.Env.port || '8080')
  }

  public get hostname(): string
  {
    return this.Env.hostname || '127.0.0.1'
  }

  public get database_driver(): string
  {
    return this.Env.database_driver || SymphonicMemoryDatabase.name
  }
  
  public get database_hostname(): string | undefined
  {
    return this.Env.database_hostname
  }
  
  public get database_port(): number | undefined
  {
    return (!! this.Env.database_port) ? parseInt(this.Env.database_port) : undefined
  }
  
  public get database_name(): string | undefined
  {
    return this.Env.database_name
  }
  
  public get database_username(): string | undefined
  {
    return this.Env.database_username
  }
  
  public get database_password(): string | undefined
  {
    return this.Env.database_password
  }
  
  public get database_fix_table_integerty(): FixTableIntegertyLevel
  {
    return (process.argv.indexOf('--force-fix-db') !== -1) ? FixTableIntegertyLevel.force : (process.argv.indexOf('--fix-db') !== -1) ? FixTableIntegertyLevel.safe : FixTableIntegertyLevel.none
  }
  
  public get cache_driver(): string
  {
    return process.env.cache_driver || SymphonicMemoryCaching.name
  }

  public get cache_hostname(): string | undefined
  {
    return process.env.cache_hostname
  }

  public get cache_port(): number | undefined
  {
    return parseInt(process.env.cache_port || 'NaN') || undefined
  }

  public get cache_database(): string | undefined
  {
    return process.env.cache_database
  }

  public get cache_username(): string | undefined
  {
    return process.env.cache_username
  }

  public get cache_password(): string | undefined
  {
    return process.env.cache_password
  }

  public get session_provider(): string
  {
    return process.env.session_provider || 'ExpressSessionManager'
  }
  
  public get session_name(): string
  {
    return process.env.session_name || os.hostname()
  }
  
  public get session_resave(): boolean
  {
    return <boolean> autoConvertString(process.env.session_resave, false)
  }
  
  public get session_save_uninitialized(): boolean
  {
    return <boolean> autoConvertString(process.env.session_save_uninitialized, false)
  }
  
  public get session_random_secret_on_bootup(): boolean
  {
    return <boolean> autoConvertString(process.env.session_random_secret_on_bootup, true)
  }
  
  public get session_unset_policy(): "destroy" | "keep"
  {
    return <"destroy" | "keep"> process.env.session_unset_policy || 'destroy'
  }
  
  public get session_secure(): boolean
  {
    return <boolean> autoConvertString(process.env.session_secure, false)
  }
  
  public get session_max_age(): number
  {
    return <number> autoConvertString(process.env.session_max_age, 86400000)
  }
  
  public get session_http_only(): boolean
  {
    return <boolean> autoConvertString(process.env.session_http_only, false)
  }
}