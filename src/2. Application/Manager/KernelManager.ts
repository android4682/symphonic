import { BetterConsole } from "@android4682/better-console";
import { AutoWireConfig } from "../../1. Domain/Interfaces/AutoWireConfig";
import { Modules } from "../../1. Domain/Interfaces/Modules";
import { InifinityObject, SpecialMap } from "@android4682/typescript-toolkit";
import path from "node:path";
import fs from "node:fs";
import { NotInitiated } from "../Errors/NotInitiated";
import { AutoWire } from "../Decorators/Autowire";

@AutoWire()
export class KernelManager
{
  private autoWireConfig: AutoWireConfig | undefined
  protected logger: BetterConsole;

  public constructor(logger: BetterConsole)
  {
    this.logger = logger
  }

  public getAutoWireConfig(): AutoWireConfig
  {
    if (! this.autoWireConfig) throw new NotInitiated('KernelManager.autoWireConfig')

    return this.autoWireConfig
  }

  public async getLiveAutoWireConfig(): Promise<AutoWireConfig>
  {
    let autowire_config_filepath = process.env.autowire_config_filepath

    if (! autowire_config_filepath) return this.autoWireConfig = <AutoWireConfig> {
      controllers: [],
      services: []
    }
    autowire_config_filepath = autowire_config_filepath.replace("{{CWD}}", process.cwd())
    
    let autoWireConfig = await import(autowire_config_filepath)
    this.autoWireConfig = <AutoWireConfig> autoWireConfig.default

    const isRunningInTypeScript = (process.argv[0].includes('ts-node'))
    
    for (let i = 0; i < Object.keys(this.autoWireConfig).length; i++) {
      const type = <keyof AutoWireConfig> Object.keys(this.autoWireConfig)[i];
      const list = this.autoWireConfig[type]
      
      for (let i = 0; i < list.length; i++) {
        const filePath = list[i];
        
        list[i] = filePath.replace("{{CWD}}", process.cwd())
        list[i] = list[i].replace("{{RUNNING_ENV}}", isRunningInTypeScript ? 'src' : 'dist')
      }
    }

    return this.autoWireConfig
  }

  public findServiceFilesInDirectory(directoryPath: string): string[]
  {
    if (! fs.existsSync(directoryPath)) {
      this.logger.warn(`Service path '${directoryPath}' doesn't exist.`)

      return []
    }

    if (fs.statSync(directoryPath).isFile()) return ((directoryPath.endsWith('.ts') && ! directoryPath.endsWith('.d.ts')) || directoryPath.endsWith('.js')) ? [directoryPath] : []

    let files = fs.readdirSync(directoryPath)

    let results: string[] = []
    for (let i = 0; i < files.length; i++) {
      const fileName = files[i];
      const filePath = path.join(directoryPath, fileName)
      
      if (fs.statSync(filePath).isDirectory()) {
        results = results.concat(this.findServiceFilesInDirectory(filePath))
      } else if ((fileName.endsWith('.ts') && ! fileName.endsWith('.d.ts')) || fileName.endsWith('.js')) {
        results.push(filePath)
      }
    }

    return results
  }

  public addService(services: SpecialMap<string, InifinityObject>, module: InifinityObject, moduleName: string, serviceFilePath?: string): typeof services
  {
    if (services.has(moduleName)) {
      this.logger.warn(`Module '${moduleName}' at path '${serviceFilePath}' is already registered. If you have a module with the same name please rename it to be registered or do not include the file in your autowire configuration.`)

      return services;
    }

    services.set(moduleName, module)

    return services;
  }
  
  public async importServiceFile(serviceFilePath: string, services: SpecialMap<string, InifinityObject> = new SpecialMap<string, InifinityObject>()): Promise<typeof services>
  {
    let modules = <Modules> await import(serviceFilePath)
    for (let i = 0; i < Object.keys(modules).length; i++) {
      const moduleName = Object.keys(modules)[i]
      if (moduleName === 'default') continue
      
      services = this.addService(services, modules[moduleName], moduleName, serviceFilePath)
    }

    return services
  }

  public async fetchServicesFromList(serviceFileList: string[], services: SpecialMap<string, InifinityObject> = new SpecialMap<string, InifinityObject>()): Promise<typeof services>
  {
    for (let i = 0; i < serviceFileList.length; i++) {
      const servicePath = serviceFileList[i];
      if (! fs.existsSync(servicePath)) {
        this.logger.warn(`Service path '${servicePath}' doesn't exist.`)
        
        continue
      }
      
      let serviceFiles = this.findServiceFilesInDirectory(servicePath)
      
      for (let i = 0; i < serviceFiles.length; i++) {
        const serviceFile = serviceFiles[i];
        services = await this.importServiceFile(serviceFile, services)
      }
    }
    
    return services
  }
}