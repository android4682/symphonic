import { Request } from "express";
import { ExpressSession } from "../../Session/ExpressSession";
import { MissingSession } from "../../Errors/MissingSession";
import { DefaultSessionManager } from "../../Abstract/Manager/Session/DefaultSessionManager";
import { AutoWire } from "../../Decorators/Autowire";
import { Container } from "../../DependencyInjection/Container";

@AutoWire()
export class ExpressSessionManager<S extends ExpressSession> extends DefaultSessionManager<S>
{
  public constructor(container: Container)
  {
    super(container, ExpressSession.name)
  }

  public get sessionClassName(): string
  {
    return ExpressSession.name
  }

  public async destorySession(uid: string, req: Request, throwTantrumWhenNotFound: boolean = false): Promise<void>
  {
    let session = this.findSessionByUID(uid)
    if (session !== null) await session.destroy(req)
    else if (throwTantrumWhenNotFound) throw new MissingSession(uid)
  }
}
