import { Request } from "express";
import "express-session";
import { DefaultSession } from "../Abstract/Session/DefaultSession";
import { AutoWire } from "../Decorators/Autowire";

@AutoWire()
export class ExpressSession extends DefaultSession
{
  public save(req: Request): Promise<void>
  {
    return new Promise<void>((resolve, reject) => {
      const session = this.getRawSession(req)
      session.data = this._session
      session.save((err) => {
        if (err) reject(err)
        else resolve()
      })
    })
  }

  public destroy(req: Request): Promise<void>
  {
    return new Promise<void>((resolve, reject) => {
      req.session.destroy((err) => {
        if (err) reject(err)
        else {
          this._session = {}
          resolve()
        }
      })
    })
  }

  public reload(req: Request): Promise<void>
  {
    return new Promise<void>((resolve, reject) => {
      req.session.reload((err) => {
        if (err) reject(err)
        else resolve()
      })
    })
  }
}