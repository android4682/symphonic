import express from "express"
import { Express, NextFunction, Request, RequestHandler, Response } from "express"
import { createServer, ServerOptions, Server } from "http";
import { createServer as createSecureServer, ServerOptions as SecureServerOptions, Server as SecureServer } from "https";
import { SpecialMap, isEmpty } from "@android4682/typescript-toolkit";
import { BetterConsole, LogLevel, getMessageAndStack } from "@android4682/better-console";
import { HttpCredentials } from "../../1. Domain/Interfaces/HttpCredentials";
import { RouteContainer, RouteHandler } from "../../2. Application/DependencyInjection/RouteContainer";
import { NotInitiated } from "../../2. Application/Errors/NotInitiated";
import { KernelManager } from "../../2. Application/Manager/KernelManager";
import { Method } from "../../1. Domain/Interfaces/Webserver";
import { AutoWire } from "../../2. Application/Decorators/Autowire";
import { Container } from "../../2. Application/DependencyInjection/Container";
import { ClassObject } from "../../1. Domain/Interfaces/ClassObject";
import { MissingRoute } from "../../2. Application/Errors/MissingRoute";
import bodyParser from "body-parser"

@AutoWire()
export class Webserver
{
  protected router: Express
  protected credentials?: HttpCredentials
  protected port: number
  protected hostname: string
  protected routeContainer: RouteContainer | undefined
  protected kernelManager: KernelManager;
  protected container: Container;
  protected logger: BetterConsole;
  protected server: SecureServer | Server | undefined = undefined
  protected initiated: boolean = false

  public constructor(
    logger: BetterConsole,
    kernelManager: KernelManager,
    container: Container,
    port: number = 8080,
    hostname: string = "127.0.0.1",
    httpsCredentials: HttpCredentials = {}
  ) {
    this.logger = logger
    this.kernelManager = kernelManager
    this.container = container
    this.router = express()
    this.credentials = httpsCredentials
    this.port = port
    this.hostname = hostname
  }
  
  /**
   * @throws { NotInitiated }
  */
 public getRouteContainer(): RouteContainer
  {
    if (! this.routeContainer) throw new NotInitiated(this.constructor.name + '.routeContainer')

    return this.routeContainer
  }
  
  /**
   * @throws { NotInitiated } 
  */
  protected getServer(): SecureServer | Server
  {
    if (! this.server) throw new NotInitiated(this.constructor.name + '.server')

    return this.server
  }

  public async initiate(): Promise<void>
  {
    if (this.initiated === true) return

    this.router.use(bodyParser.urlencoded({
      extended: true
    }));
    this.router.use(bodyParser.json());

    await this.initiateRoutes()
    this.setAutoWiredEndpoints()
    await this.startWebServer()
    
    this.initiated = true
  }

  protected async initiateRoutes(): Promise<void>
  {
    let autoWireConfig = this.kernelManager.getAutoWireConfig()

    let contollerServices = <SpecialMap<string, ClassObject<any>>> await this.kernelManager.fetchServicesFromList(autoWireConfig.controllers)
    
    this.routeContainer = new RouteContainer(this.logger, this.container, contollerServices)

    this.logger.dir({routeContainer: this.routeContainer}, undefined, LogLevel.dev)
  }

  public setSetting(setting: string, value: any): void
  {
    this.router.set(setting, value)
  }

  public registerMiddleware(...handlers: RequestHandler[]): void
  {
    this.router.use(...handlers)
  }

  public registerPathMiddleware(path: string, ...handlers: RequestHandler[]): void
  {
    this.router.use(path, ...handlers)
  }

  protected findAnyRouteHandler(url: string, method: Method): RouteHandler | MissingRoute
  {
    let originalError: MissingRoute | undefined = undefined

    let result = this.getRouteContainer().findRouteHandler(url, method, false)
    if (result instanceof MissingRoute) originalError = result
    else return result

    result = this.getRouteContainer().findRouteHandler(url, 'all', false)
    if (! (result instanceof MissingRoute)) return result

    result = this.getRouteContainer().findRouteHandler(url, undefined, false)
    if (! (result instanceof MissingRoute)) return result

    return originalError
  }

  protected findRouteHandler(url: string, method: Method): RouteHandler | MissingRoute
  {
    let originalError: MissingRoute | undefined = undefined

    let result = this.findAnyRouteHandler(url, method)
    if (result instanceof MissingRoute) originalError = result
    else return result

    result = this.findAnyRouteHandler('404', method)
    if (result instanceof MissingRoute) throw originalError
    else return result
  }

  protected setAutoWiredEndpoints(): void
  {
    this.router.all("*", (req: Request, res: Response, next: NextFunction) => {
      // TODO: If in dev mode show error otherwise show 404
      this.logger.dir({url: req.url, method: req.method.toLowerCase()})
      let method = <Method> req.method.toLowerCase()

      let result = this.findRouteHandler(req.url, method)
      if (result instanceof MissingRoute) throw result
      else return result[0][result[1]].call(result[0], req, res, next)
    })

    this.router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
      if (err instanceof MissingRoute) throw err
      let [message, stack] = getMessageAndStack(err)
      this.logger.error(message)
      this.logger.dir(stack, undefined, LogLevel.error)

      let method = <Method> req.method.toLowerCase()

      let result = this.findRouteHandler('500', method)
      if (result instanceof MissingRoute) throw result
      else return result[0][result[1]].call(result[0], req, res, next)
    })
  }

  protected async startWebServer(): Promise<void>
  {
    return new Promise<void>((resolve, reject) => {
      let cb = () => {
        this.logger.success(`Web server running at ${this.hostname}:${this.port}`)
        resolve()
      }
      this.logger.dev("isHttps: " + this.isHttps())
      if (this.isHttps()) this.server = createSecureServer(this.getSecureServerOptions(), this.router).listen(this.port, this.hostname, cb)
      else this.server = createServer(this.getServerOptions(), this.router).listen(this.port, this.hostname, cb)
    })
  }

  protected getServerOptions(): ServerOptions
  {
    return <ServerOptions> {}
  }

  protected getSecureServerOptions(): SecureServerOptions
  {
    return <SecureServerOptions> {
      ...this.credentials
    }
  }

  protected isHttps(): boolean
  {
    return (!isEmpty(this.credentials) && !isEmpty(this.credentials?.key) && !isEmpty(this.credentials?.cert))
  }

  /**
   * @throws { NotInitiated }
   */
  public getBaseURL(): string
  {
    return `http${(this.isHttps()) ? 's' : ''}://${this.hostname}:${this.port}`
  }

  public stopServer(): Promise<void>
  {
    return new Promise<void>((resolve, reject) => {
      this.getServer().close((err) => {
        if (err) {
          reject(err)
        } else {
          this.server = undefined
          resolve()
        }
      })
    })
  }
}
