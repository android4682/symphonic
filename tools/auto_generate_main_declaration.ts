import fs from "node:fs";
import path from "node:path";
import { BetterConsole } from "@android4682/better-console";
import dotenv from "dotenv"

const console = new BetterConsole()
const tsConfigFilePath = path.join(__dirname, '..', 'src', 'tsconfig.json')

interface TSConfig
{
  compilerOptions: {
    outDir?: string
  }
}

let tsconfig: TSConfig | undefined
async function getTsConfig(): Promise<TSConfig>
{
  if (!! tsconfig) return tsconfig

  let _tsconfig = await import(tsConfigFilePath)

  return tsconfig = <TSConfig> _tsconfig.default
}

let outDir: string | undefined
function getOutDir(): string
{
  if (! outDir) throw new Error('outDir isn\'t fetched yet.')

  return outDir
}

function getMainDeclarationFilePath(outDir: string): string
{
  return path.join(outDir, 'main')
}

async function fetchOutDir(): Promise<string>
{
  let tsconfig = await getTsConfig()

  if (! tsconfig.compilerOptions.outDir) throw new Error(`Missing compilerOptions.outDir in "${tsConfigFilePath}".`)

  return outDir = tsconfig.compilerOptions.outDir
}

function getTsConfigParentFolderPath(): string
{
  return path.normalize(path.join(tsConfigFilePath, '..'))
}

function findDeclarationFiles(filePath: string, ext: string = '.js'): string[]
{
  let res: string[] = []
  if (! fs.existsSync(filePath)) {
    console.warn(`"${filePath}" does not exist, skipping.`)
    
    return res
  }
  
  let stat = fs.statSync(filePath)
  if (stat.isFile()) {
    if (filePath.endsWith(getMainDeclarationFilePath('.') + ext)) console.debug(`Not adding "${getMainDeclarationFilePath(getOutDir()) + ext}" to list, skipping.`)
    else if (filePath.endsWith(ext)) res.push(filePath)
    else console.debug(`File "${filePath}" is not a '.js' file, skipping.`)
  } else if (stat.isDirectory()) {
    let subFiles = fs.readdirSync(filePath)
    for (let i = 0; i < subFiles.length; i++) {
      const subFileName = subFiles[i];
      const subFilePath = path.join(filePath, subFileName)
      res = res.concat(findDeclarationFiles(subFilePath))
    }
  } else {
    console.warn(`Path "${filePath}" is not a file nor a directory, skipping...`)
  }

  return res
}

async function main(): Promise<void>
{
  dotenv.config({
    "path": path.join(process.cwd(), ".env")
  })

  let loglevel: number = parseInt(process.env.loglevel || '3')
  console.logLevel = loglevel
  let distFolder = await fetchOutDir()

  process.chdir(getTsConfigParentFolderPath());
  process.chdir(distFolder);
  let declarationFilePaths = findDeclarationFiles('.')

  let exportString = ``
  for (let i = 0; i < declarationFilePaths.length; i++) {
    const filePath = declarationFilePaths[i];
    const unixPathFormat = filePath.split(path.sep).join(path.posix.sep)
    exportString += `export * from './${unixPathFormat}';\n`
  }

  let mainDeclarationFilePath = path.join(distFolder, 'main.d.ts')
  let mainJavaScriptFilePath = path.join(distFolder, 'main.js')
  fs.writeFileSync(mainJavaScriptFilePath, exportString)
  fs.writeFileSync(mainDeclarationFilePath, exportString)

  console.success(`Main declartion file written to "${mainDeclarationFilePath}".`)
  console.success(`Main js file written to "${mainJavaScriptFilePath}".`)
}

main().then(() => process.exit(0)).catch((e) => {
  throw e
})